import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.*;
import java.util.logging.Logger;

class Server implements Runnable {

    private static final int PORT = 8888;
    private static final Logger logger = Logger.getLogger(Server.class.getName());
    private final List<ChangeRequest> pendingChanges = new LinkedList<>();
    private final Map<SocketChannel, List<ByteBuffer>> pendingData = new HashMap<>();
    private InetAddress ADDRESS = InetAddress.getByName("localhost");
    private Selector selector;
    private ByteBuffer readBuffer = ByteBuffer.allocate(8192);
    private Handler handler;
    private ArrayList<Client> clients = new ArrayList<>();

    private Server(Handler handler) throws IOException {
        this.selector = this.initSelector();
        this.handler = handler;
    }

    public static void main(String[] args) {
        try {
            Handler handler = new Handler();
            new Thread(handler).start();
            new Thread(new Server(handler)).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void send(SocketChannel socket, byte[] data) {
        synchronized (this.pendingChanges) {
            this.pendingChanges.add(new ChangeRequest(socket, ChangeRequest.CHANGEOPS, SelectionKey.OP_WRITE));
            synchronized (this.pendingData) {
                List<ByteBuffer> queue = this.pendingData.get(socket);
                if (queue == null) {
                    queue = new ArrayList<>();
                    this.pendingData.put(socket, queue);
                }
                queue.add(ByteBuffer.wrap(data));
            }
        }
        this.selector.wakeup();
    }

    public void run() {
        //noinspection InfiniteLoopStatement
        for (; ; ) {
            try {
                synchronized (this.pendingChanges) {
                    for (ChangeRequest change : this.pendingChanges) {
                        switch (change.type) {
                            case ChangeRequest.CHANGEOPS:
                                SelectionKey key = change.socket.keyFor(this.selector);
                                key.interestOps(change.ops);
                        }
                    }
                    this.pendingChanges.clear();
                }
                this.selector.select();
                Iterator<SelectionKey> selectedKeys = this.selector.selectedKeys().iterator();

                while (selectedKeys.hasNext()) {
                    SelectionKey key = selectedKeys.next();
                    selectedKeys.remove();
                    if (!key.isValid()) {
                        continue;
                    }
                    if (key.isAcceptable()) {
                        this.accept(key);
                    } else if (key.isReadable()) {
                        this.read(key);
                    } else if (key.isWritable()) {
                        this.write(key);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void accept(SelectionKey key) throws IOException {
        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
        SocketChannel socketChannel = serverSocketChannel.accept();
        Client client = new Client();
        socketChannel.configureBlocking(false);
        socketChannel.register(this.selector, SelectionKey.OP_READ, client);
    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();
        this.readBuffer.clear();
        int numRead;
        try {
            numRead = socketChannel.read(this.readBuffer);
        } catch (IOException e) {
            //noinspection SuspiciousMethodCalls
            clients.remove(key.attachment());
            logger.info("Client " + ((Client) key.attachment()).getID() + " left without saying goodbye :(");
            key.cancel();
            socketChannel.close();
            return;
        }
        if (numRead == -1) {
            //noinspection SuspiciousMethodCalls
            clients.remove(key.attachment());
            logger.info("Client " + ((Client) key.attachment()).getID() + " left without saying goodbye :(");
            key.channel().close();
            key.cancel();
            return;
        }
        this.handler.processData(this, socketChannel, key, clients, this.readBuffer);
    }

    private void write(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();
        synchronized (this.pendingData) {
            List<ByteBuffer> queue = this.pendingData.get(socketChannel);
            while (!queue.isEmpty()) {
                ByteBuffer buf = queue.get(0);
                socketChannel.write(buf);
                if (buf.remaining() > 0) {
                    break;
                }
                queue.remove(0);
            }
            if (queue.isEmpty()) {
                key.interestOps(SelectionKey.OP_READ);
            }
        }
    }

    private Selector initSelector() throws IOException {
        Selector socketSelector = SelectorProvider.provider().openSelector();
        ServerSocketChannel serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);
        serverChannel.socket().setReuseAddress(true);
        InetSocketAddress isa = new InetSocketAddress(ADDRESS, PORT);
        serverChannel.socket().bind(isa);
        serverChannel.register(socketSelector, SelectionKey.OP_ACCEPT);
        return socketSelector;
    }
}
