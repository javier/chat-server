import java.nio.ByteBuffer;

class Message {

    final int PROTOCOL_TAG = 0x11223344;
    final int HEADER_SIZE = 22;
    short cmd;
    int length;
    int originID;
    int destinationID;
    int msgID;
    byte[] payload;

    Message() {
    }

    Message(int l, short cmd, int mID, int dID, int oID, byte[] p) {
        this.length = l;// 4
        this.cmd = cmd;// 2
        this.msgID = mID;// 4
        this.destinationID = dID;// 4
        this.originID = oID;// 4
        this.payload = p;
    }

    void parseMessageFromByteBuffer(ByteBuffer buffer) {
        buffer.clear();
        buffer.getInt();//4
        this.length = buffer.getInt();// 4
        this.cmd = buffer.getShort();// 2
        this.msgID = buffer.getInt();// 4
        this.destinationID = buffer.getInt();// 4
        this.originID = buffer.getInt();// 4
        payload = new byte[this.length];
        buffer.get(payload);
    }

    ByteBuffer constructByteBuffer() {
        ByteBuffer buffer = ByteBuffer.allocate(HEADER_SIZE + this.length);
        buffer.clear();
        buffer.putInt(PROTOCOL_TAG);
        buffer.putInt(this.length);
        buffer.putShort(this.cmd);
        buffer.putInt(this.msgID);
        buffer.putInt(this.destinationID);
        buffer.putInt(this.originID);
        buffer.put(payload, 0, this.length);
        return buffer;
    }
}
