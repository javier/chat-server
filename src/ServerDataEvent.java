import java.nio.channels.SocketChannel;

class ServerDataEvent {
    Server server;
    SocketChannel socket;
    byte[] data;

    ServerDataEvent(Server server, SocketChannel socket, byte[] data) {
        this.server = server;
        this.socket = socket;
        this.data = data;
    }
}