import java.nio.channels.SocketChannel;

class Client {

    private final ThreadLocal<SocketChannel> socket = new ThreadLocal<>();
    private int id;
    private boolean logged;

    Client() {
    }

    int getID() {
        return this.id;
    }

    void setID(int id) {
        this.id = id;
    }

    SocketChannel getSocket() {
        return this.socket.get();
    }

    void setSocket(SocketChannel socket) {
        this.socket.set(socket);
    }

    boolean getStatus() {
        return this.logged;
    }

    void setStatus(boolean logged) {
        this.logged = logged;
    }


}
