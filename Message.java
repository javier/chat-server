import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

public class Message {

    final int protocolTag = 0x11223344;
    short cmd;
    int length;
    int originID;
    int destinationID;
    int msgID;
    byte[] payload;

    public Message() {
    }

    public Message(int l, short cmd, int mID, int dID, int oID, byte[] p) {
        this.length = l;// 4
        this.cmd = cmd;// 2
        this.msgID = mID;// 4
        this.destinationID = dID;// 4
        this.originID = oID;// 4
        this.payload = p;
    }

    public void parseMessageFromByteBuffer(ByteBuffer buffer) {
        buffer.clear();
        buffer.getInt();
        this.length = buffer.getInt();// 4
        this.cmd = buffer.getShort();// 2
        this.msgID = buffer.getInt();// 4
        this.destinationID = buffer.getInt();// 4
        this.originID = buffer.getInt();// 4
        payload = new byte[this.length];
        try {
            String value = new String(payload, "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        buffer.get(payload);
    }

    public ByteBuffer constructByteBuffer() {
        ByteBuffer buffer = ByteBuffer.allocate(22 + this.length);
        buffer.clear();
        buffer.putInt(this.protocolTag);
        buffer.putInt(this.length);
        buffer.putShort(this.cmd);
        buffer.putInt(this.msgID);
        buffer.putInt(this.destinationID);
        buffer.putInt(this.originID);
        buffer.put(payload, 0, this.length);
        return buffer;
    }
}
