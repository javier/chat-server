import java.nio.channels.SocketChannel;

public class Client {

    int id;
    SocketChannel socket;
    boolean logged;

    public Client() {
    }

    public void setID(int id) {
        this.id = id;
    }

    public SocketChannel getSocket() {
        return this.socket;
    }

    public void setSocket(SocketChannel socket) {
        this.socket = socket;
    }

    public boolean getStatus() {
        return this.logged;
    }

    public void setStatus(boolean logged) {
        this.logged = logged;
    }

}
