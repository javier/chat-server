import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

public class Handler implements Runnable {

    private static final short LOGIN = 0x00;
    private static final short LOGIN_RESPONSE = 0x01;
    private static final short SEND_MESSAGE = 0x02;
    private static final short SEND_MESSAGE_RESPONSE = 0x03;
    private static final short LOGOUT = 0x04;
    private static final short LOGOUT_RESPONSE = 0x05;

    private static final Logger logger = Logger.getLogger(Server.class.getName());
    private List<ServerDataEvent> queue = new LinkedList<>();

    public Client findClient(final List<Client> clients, final int id) {
        return clients.stream().filter(c -> c.id == id).findFirst().orElse(null);
    }

    private void performOptionFromReceivedMessage(Server server, ArrayList<Client> clients, SocketChannel socket, Message msg, SelectionKey key) {
        Message loginResponseMessage, logoutResponseMessage, sendMessageResponse;
        byte[] payload = "failure".getBytes(StandardCharsets.US_ASCII);
        switch (msg.cmd) {
            case LOGIN:
                if (findClient(clients, msg.originID) != null) {
                    logger.info("ID " + msg.originID + " is already logged in");
                } else {
                    ((Client) key.attachment()).setID(msg.originID);
                    ((Client) key.attachment()).setStatus(true);//online
                    ((Client) key.attachment()).setSocket(socket);
                    clients.add((Client) key.attachment());
                    payload = "success".getBytes(StandardCharsets.US_ASCII);
                    logger.info("Client " + msg.originID + " is now registered to the server");
                }
                loginResponseMessage = new Message(7, LOGIN_RESPONSE, msg.msgID, msg.originID, 0, payload);
                sendMessage(server, socket, loginResponseMessage.constructByteBuffer().array());
                break;
            case SEND_MESSAGE:
                Client c = findClient(clients, msg.destinationID);
                if (c != null && socket == findClient(clients, msg.originID).getSocket()) {
                    payload = "success".getBytes(StandardCharsets.US_ASCII);
                    sendMessageResponse = new Message(7, SEND_MESSAGE_RESPONSE, msg.msgID, msg.originID, 0, payload);
                    sendMessage(server, socket, sendMessageResponse.constructByteBuffer().array());
                    Message message = new Message(msg.length, SEND_MESSAGE, msg.msgID, msg.destinationID, msg.originID, msg.payload);
                    sendMessage(server, c.socket, message.constructByteBuffer().array());
                } else {
                    sendMessageResponse = new Message(7, SEND_MESSAGE_RESPONSE, msg.msgID, msg.originID, 0, payload);
                    sendMessage(server, socket, sendMessageResponse.constructByteBuffer().array());
                }
                break;
            case LOGOUT:
                //logger.info("Client " + msg.originID + " is disconnected");
                if (((Client) key.attachment()).getStatus()) {
                    payload = "success".getBytes(StandardCharsets.US_ASCII);
                    clients.remove(key.attachment());
                }
                logoutResponseMessage = new Message(7, LOGOUT_RESPONSE, msg.msgID, msg.originID, 0, payload);
                sendMessage(server, socket, logoutResponseMessage.constructByteBuffer().array());
                break;
            default:
                break;
        }
    }

    public void sendMessage(Server server, SocketChannel socket, byte[] message) {
        synchronized (queue) {
            queue.add(new ServerDataEvent(server, socket, message));
            queue.notify();
        }
    }

    public void processData(Server server, SocketChannel socket, SelectionKey key, ArrayList<Client> clients, ByteBuffer data) {
        Message newMessage = new Message();
        newMessage.parseMessageFromByteBuffer(data);
        performOptionFromReceivedMessage(server, clients, socket, newMessage, key);
    }

    public void run() {
        ServerDataEvent dataEvent;

        while (true) {
            // Wait for data to become available
            synchronized (queue) {
                while (queue.isEmpty()) {
                    try {
                        queue.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                dataEvent = queue.remove(0);
            }
            // Return to sender
            dataEvent.server.send(dataEvent.socket, dataEvent.data);
        }
    }
}
